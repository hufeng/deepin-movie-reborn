<?xml version="1.0" ?><!DOCTYPE TS><TS language="hr" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <source>Invalid folder</source>
        <translation>Neispravna mapa</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Otvori mapu</translation>
    </message>
    <message>
        <source>Network disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You don&apos;t have permission to operate this folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Auto add similar files to play</source>
        <translation>Dodaj slične datoteke za izvođenje</translation>
    </message>
    <message>
        <source>Clear playlist when exit</source>
        <translation>Očisti listu izvođenja prilikom izlaska</translation>
    </message>
    <message>
        <source>Show video preview on mouseover</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open a new player for each file played</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Pause when minimized</source>
        <translation>Pauziraj pri minimiziranju</translation>
    </message>
    <message>
        <source>Remember playback position</source>
        <translation>Zapamti poziciju reprodukcije</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Putanja</translation>
    </message>
    <message>
        <source>Basic</source>
        <translation>Osnovno</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>Reprodukcija</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Snimak zaslona</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Prečaci</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Datoteka</translation>
    </message>
    <message>
        <source>Frame/Sound</source>
        <translation>Okvir/zvuk</translation>
    </message>
    <message>
        <source>Playback</source>
        <translation>Reprodukcija</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>Podnapis</translation>
    </message>
    <message>
        <source>Font Style</source>
        <translation>Stil fonta</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>Obnovi zadano</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Otvori datoteku</translation>
    </message>
    <message>
        <source>Open next</source>
        <translation>Otvori sljedeće</translation>
    </message>
    <message>
        <source>Open previous</source>
        <translation>Otvori prijašnje</translation>
    </message>
    <message>
        <source>Mini mode</source>
        <translation>Mini prikaz</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Utišaj</translation>
    </message>
    <message>
        <source>Next frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Volume down</source>
        <translation>Smanji glasnoću zvuka</translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation>Pojačaj glasnoću zvuka</translation>
    </message>
    <message>
        <source>Speed up</source>
        <translation>Ubrzaj</translation>
    </message>
    <message>
        <source>Speed down</source>
        <translation>Uspori</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Cijeli zaslon</translation>
    </message>
    <message>
        <source>Pause/Play</source>
        <translation>Pauza/Reprodukcija</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Popis izvođenja</translation>
    </message>
    <message>
        <source>Reset speed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Rewind</source>
        <translation>Premotaj natrag</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Premotaj naprijed</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>0.5s backward</source>
        <translation>0.5s unatrag</translation>
    </message>
    <message>
        <source>0.5s forward</source>
        <translation>0.5s naprijed</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Font</translation>
    </message>
    <message>
        <source>Exit fullscreen/mini mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Movie</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Settings</source>
        <translation>Postavke</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Pomoć</translation>
    </message>
    <message>
        <source>Display shortcuts</source>
        <translation>Prikaži prečace</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>Informacije o filmu</translation>
    </message>
    <message>
        <source>Burst shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size</source>
        <translation>Veličina</translation>
    </message>
    <message>
        <source>Movie is a full-featured video player, supporting playing local and streaming media in multiple video formats.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>U redu</translation>
    </message>
    <message>
        <source>Default play mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show thumbnails in progress bar</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UrlDialog</name>
    <message>
        <source>Cancel</source>
        <translation>Otkaži</translation>
    </message>
    <message>
        <source>Please enter the URL:</source>
        <translation>Molim unesite URL:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>U redu</translation>
    </message>
</context>
<context>
    <name>dmr::ActionFactory</name>
    <message>
        <source>Settings</source>
        <translation>Postavke</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Cijeli zaslon</translation>
    </message>
    <message>
        <source>Always on Top</source>
        <translation>Uvijek na vrhu</translation>
    </message>
    <message>
        <source>Empty playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>Informacije o filmu</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Otvori datoteku</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Otvori mapu</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation>Otvori URL</translation>
    </message>
    <message>
        <source>Open CD/DVD</source>
        <translation>Otvori CD/DVD</translation>
    </message>
    <message>
        <source>Mini Mode</source>
        <translation>Mini način</translation>
    </message>
    <message>
        <source>Play Mode</source>
        <translation>Način reprodukcije</translation>
    </message>
    <message>
        <source>Order Play</source>
        <translation>Redoslijed izvođenja</translation>
    </message>
    <message>
        <source>Shuffle Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single Loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List Loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Frame</source>
        <translation>Sličica</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Uobičajeno</translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation>U smjeru kazaljke na satu</translation>
    </message>
    <message>
        <source>Counterclockwise</source>
        <translation>Suprotno od smjera kazaljke na satu</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Zvuk</translation>
    </message>
    <message>
        <source>Channel</source>
        <translation>Kanal</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>Stereo</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>Lijevi kanal</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>Desni kanal</translation>
    </message>
    <message>
        <source>Track</source>
        <translation>Zapis</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>Podnapis</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Učitaj</translation>
    </message>
    <message>
        <source>Online Search</source>
        <translation>Mrežna pretraga</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Odaberi</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation>Sakrij</translation>
    </message>
    <message>
        <source>Encodings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Snimak zaslona</translation>
    </message>
    <message>
        <source>Film Screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Burst Shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Popis izvođenja</translation>
    </message>
    <message>
        <source>Film Info</source>
        <translation>Informacije o filmu</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display in file manager</source>
        <translation>Prikaži u upravitelju datotekama</translation>
    </message>
    <message>
        <source>Next Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open screenshot folder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::BurstScreenshotsDialog</name>
    <message>
        <source>Duration: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resolution: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size: %1</source>
        <translation>Veličina: %1</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Spremi</translation>
    </message>
</context>
<context>
    <name>dmr::MainWindow</name>
    <message>
        <source>Load successfully</source>
        <translation>Uspješno učitano</translation>
    </message>
    <message>
        <source>Load failed</source>
        <translation>Neuspjelo učitavanje</translation>
    </message>
    <message>
        <source>Buffering...</source>
        <translation>Učitavanje...</translation>
    </message>
    <message>
        <source>No device found</source>
        <translation>NIje pronađen uređaj</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Otvori mapu</translation>
    </message>
    <message>
        <source>All videos (%1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Stereo</source>
        <translation>Stereo</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>Lijevi kanal</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>Desni kanal</translation>
    </message>
    <message>
        <source>Parse failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mute</source>
        <translation>Utišaj</translation>
    </message>
    <message>
        <source>Volume: %1%</source>
        <translation>Glasnoća: %1%</translation>
    </message>
    <message>
        <source>Track: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle %1: %2s</source>
        <translation>Podnapis %1: %2s</translation>
    </message>
    <message>
        <source>delayed</source>
        <translation>odgođeno</translation>
    </message>
    <message>
        <source>advanced</source>
        <translation>napredno</translation>
    </message>
    <message>
        <source>Speed: %1x</source>
        <translation>Brzina: %1x</translation>
    </message>
    <message>
        <source>Subtitle (*.ass *.aqt *.jss *.gsub *.ssf *.srt *.sub *.ssa *.smi *.usf *.idx)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View</source>
        <translation>Pogled</translation>
    </message>
    <message>
        <source>Saved to</source>
        <translation>Spremljeno u</translation>
    </message>
    <message>
        <source>The screenshot is saved</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Failed to save the screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file: %1</source>
        <translation>Neispravna datoteka: %1</translation>
    </message>
    <message>
        <source>No matching online subtitles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot open file or stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file</source>
        <translation>Neispravna datoteka</translation>
    </message>
    <message>
        <source>No video file found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open File</source>
        <translation>Otvori datoteku</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Taking the screenshots, please wait...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>U redu</translation>
    </message>
    <message>
        <source>Please insert a CD/DVD</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reading DVD files...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>4K video may be stuck</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please load the video first</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation>Redoslijed izvođenja</translation>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The CD/DVD has been ejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%2 %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (*)(%2 %1)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfo</name>
    <message>
        <source>%1G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfoDialog</name>
    <message>
        <source>Film info</source>
        <translation>Informacije o filmu</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>Razlučivost</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Trajanje</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Vrsta</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Veličina</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Putanja</translation>
    </message>
    <message>
        <source>Codec info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 kbps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>FPS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 fps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Proportion</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio digit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 bits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sampling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1hz</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MpvProxy</name>
    <message>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlayItemWidget</name>
    <message>
        <source>The file does not exist</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlaylistWidget</name>
    <message>
        <source>Playlist</source>
        <translation>Popis izvođenja</translation>
    </message>
    <message>
        <source>%1 videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Empty</source>
        <translation>Prazno</translation>
    </message>
</context>
<context>
    <name>dmr::Settings</name>
    <message>
        <source>%1/Movie%2.jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1/Movie%2(%3).jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation>Redoslijed izvođenja</translation>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::ToolboxProxy</name>
    <message>
        <source>Play</source>
        <translation>Reprodukcija</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>Prijašnje</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>Slijedeće</translation>
    </message>
    <message>
        <source>Subtitles</source>
        <translation>Podnapisi</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Popis izvođenja</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Cijeli zaslon</translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation>Reprodukcija/Pauza</translation>
    </message>
    <message>
        <source>play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>prev</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>next</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>fs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Exit fullscreen</source>
        <translation>Izađi iz cijelog zaslona</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Pauziraj</translation>
    </message>
</context>
</TS>