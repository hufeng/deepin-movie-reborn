<?xml version="1.0" ?><!DOCTYPE TS><TS language="sl" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <source>Invalid folder</source>
        <translation>Neveljavna mapa</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Odpri mapo</translation>
    </message>
    <message>
        <source>Network disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You don&apos;t have permission to operate this folder</source>
        <translation>Nimate dovoljenja za upravljanje te mape</translation>
    </message>
    <message>
        <source>Auto add similar files to play</source>
        <translation>Samodejno dodaj podobne datoteke na seznam</translation>
    </message>
    <message>
        <source>Clear playlist when exit</source>
        <translation>Ob izhodu izprazni seznam predvajanja</translation>
    </message>
    <message>
        <source>Show video preview on mouseover</source>
        <translation>Ob primiku z miško pokaži predogled posnetka</translation>
    </message>
    <message>
        <source>Open a new player for each file played</source>
        <translation>Odpri nov predvajalnik za vsako datoteko</translation>
    </message>
    <message>
        <source>Pause when minimized</source>
        <translation>Ustavi, ko se pomanjša</translation>
    </message>
    <message>
        <source>Remember playback position</source>
        <translation>Zapomni si položaj predvajane vsebine</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Pot</translation>
    </message>
    <message>
        <source>Basic</source>
        <translation>Osnove</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>Predvajaj</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Slika zaslona</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Bližnjice</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Datoteka</translation>
    </message>
    <message>
        <source>Frame/Sound</source>
        <translation>Slika/zvok</translation>
    </message>
    <message>
        <source>Playback</source>
        <translation>Predvajanje</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>Podnapisi</translation>
    </message>
    <message>
        <source>Font Style</source>
        <translation>Stil pisave</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>Obnovi privzeto</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Odpri datoteko</translation>
    </message>
    <message>
        <source>Open next</source>
        <translation>Odpri naslednje</translation>
    </message>
    <message>
        <source>Open previous</source>
        <translation>Odpri prejšnje</translation>
    </message>
    <message>
        <source>Mini mode</source>
        <translation>Pomanjšano</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Utišaj</translation>
    </message>
    <message>
        <source>Next frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Volume down</source>
        <translation>Tišje</translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation>Glasneje</translation>
    </message>
    <message>
        <source>Speed up</source>
        <translation>Pospeši</translation>
    </message>
    <message>
        <source>Speed down</source>
        <translation>Počasneje</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Celozaslonski način</translation>
    </message>
    <message>
        <source>Pause/Play</source>
        <translation>Premor/predvajaj</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Seznam predvajanja</translation>
    </message>
    <message>
        <source>Reset speed</source>
        <translation>Ponastavi hitrost</translation>
    </message>
    <message>
        <source>Rewind</source>
        <translation>Prevrti nazaj</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Previj naprej</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation>Slika filma</translation>
    </message>
    <message>
        <source>0.5s backward</source>
        <translation>0,5 s nazaj</translation>
    </message>
    <message>
        <source>0.5s forward</source>
        <translation>0,5 s naprej</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Pisava</translation>
    </message>
    <message>
        <source>Exit fullscreen/mini mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Movie</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Settings</source>
        <translation>Nastavitve</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Pomoč</translation>
    </message>
    <message>
        <source>Display shortcuts</source>
        <translation>Prikaži bližnjice</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>Podatki o filmu</translation>
    </message>
    <message>
        <source>Burst shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <source>Movie is a full-featured video player, supporting playing local and streaming media in multiple video formats.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>V redu</translation>
    </message>
    <message>
        <source>Default play mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show thumbnails in progress bar</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UrlDialog</name>
    <message>
        <source>Cancel</source>
        <translation>Prekliči</translation>
    </message>
    <message>
        <source>Please enter the URL:</source>
        <translation>Prosim, vnesite URL-naslov:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>V redu</translation>
    </message>
</context>
<context>
    <name>dmr::ActionFactory</name>
    <message>
        <source>Settings</source>
        <translation>Nastavitve</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Celozaslonski način</translation>
    </message>
    <message>
        <source>Always on Top</source>
        <translation>Vedno na vrhu</translation>
    </message>
    <message>
        <source>Empty playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>Podatki o filmu</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Odpri datoteko</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Odpri mapo</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation>Odpri URL naslov</translation>
    </message>
    <message>
        <source>Open CD/DVD</source>
        <translation>Odpri CD/DVD</translation>
    </message>
    <message>
        <source>Mini Mode</source>
        <translation>Pomanjšano</translation>
    </message>
    <message>
        <source>Play Mode</source>
        <translation>Način predvajanja</translation>
    </message>
    <message>
        <source>Order Play</source>
        <translation>Vrstni red predvajanja </translation>
    </message>
    <message>
        <source>Shuffle Play</source>
        <translation>Mešanje predvajanja</translation>
    </message>
    <message>
        <source>Single Play</source>
        <translation>Enojno predvajanje</translation>
    </message>
    <message>
        <source>Single Loop</source>
        <translation>Enojna zanka</translation>
    </message>
    <message>
        <source>List Loop</source>
        <translation>Zanka seznamov</translation>
    </message>
    <message>
        <source>Frame</source>
        <translation>Slika</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Privzeto</translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation>V smeri urinega kazalca</translation>
    </message>
    <message>
        <source>Counterclockwise</source>
        <translation>Proti urinemu kazalcu</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Zvok</translation>
    </message>
    <message>
        <source>Channel</source>
        <translation>Kanal</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>Stereo</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>Levi kanal</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>Desni kanal</translation>
    </message>
    <message>
        <source>Track</source>
        <translation>Skladba</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>Podnapisi</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Naloži</translation>
    </message>
    <message>
        <source>Online Search</source>
        <translation>Spletno brskanje</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Izberi</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation>Skrij</translation>
    </message>
    <message>
        <source>Encodings</source>
        <translation>Encodings</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Slika zaslona</translation>
    </message>
    <message>
        <source>Film Screenshot</source>
        <translation>Slika filma</translation>
    </message>
    <message>
        <source>Burst Shooting</source>
        <translation>Intervalno slikanje</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Seznam predvajanja</translation>
    </message>
    <message>
        <source>Film Info</source>
        <translation>Podatki o filmu</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display in file manager</source>
        <translation>Prikaži v upravitelju datotek</translation>
    </message>
    <message>
        <source>Next Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open screenshot folder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::BurstScreenshotsDialog</name>
    <message>
        <source>Duration: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resolution: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save</source>
        <translation>Shrani</translation>
    </message>
</context>
<context>
    <name>dmr::MainWindow</name>
    <message>
        <source>Load successfully</source>
        <translation>Uspešno naloženo</translation>
    </message>
    <message>
        <source>Load failed</source>
        <translation>Nalaganje neuspešno</translation>
    </message>
    <message>
        <source>Buffering...</source>
        <translation>Nabiram podatke...</translation>
    </message>
    <message>
        <source>No device found</source>
        <translation>Ne najdem naprave</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Odpri mapo</translation>
    </message>
    <message>
        <source>All videos (%1)</source>
        <translation>Vsi posnetki (%1)</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>Stereo</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>Levi kanal</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>Desni kanal</translation>
    </message>
    <message>
        <source>Parse failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mute</source>
        <translation>Utišaj</translation>
    </message>
    <message>
        <source>Volume: %1%</source>
        <translation>Glasnost: %1%</translation>
    </message>
    <message>
        <source>Track: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle %1: %2s</source>
        <translation>Podnapisi %1: %2s</translation>
    </message>
    <message>
        <source>delayed</source>
        <translation>zamaknjeno</translation>
    </message>
    <message>
        <source>advanced</source>
        <translation>napredno</translation>
    </message>
    <message>
        <source>Speed: %1x</source>
        <translation>Hitrost: %1x</translation>
    </message>
    <message>
        <source>Subtitle (*.ass *.aqt *.jss *.gsub *.ssf *.srt *.sub *.ssa *.smi *.usf *.idx)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View</source>
        <translation>Prikaži</translation>
    </message>
    <message>
        <source>Saved to</source>
        <translation>Shranjeno v</translation>
    </message>
    <message>
        <source>The screenshot is saved</source>
        <translation>Slika je shranjena</translation>
    </message>
    <message>
        <source>Failed to save the screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file: %1</source>
        <translation>Neveljavna datoteka: %1</translation>
    </message>
    <message>
        <source>No matching online subtitles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot open file or stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file</source>
        <translation>Neveljavna datoteka</translation>
    </message>
    <message>
        <source>No video file found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open File</source>
        <translation>Odpri datoteko</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation>Slika filma</translation>
    </message>
    <message>
        <source>Taking the screenshots, please wait...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>V redu</translation>
    </message>
    <message>
        <source>Please insert a CD/DVD</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reading DVD files...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>4K video may be stuck</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please load the video first</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation>Vrstni red predvajanja </translation>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The CD/DVD has been ejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%2 %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (*)(%2 %1)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfo</name>
    <message>
        <source>%1G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfoDialog</name>
    <message>
        <source>Film info</source>
        <translation>Podatki o filmu</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>Ločljivost</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Trajanje</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Vrsta</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Pot</translation>
    </message>
    <message>
        <source>Codec info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 kbps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>FPS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 fps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Proportion</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio digit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 bits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sampling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1hz</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MpvProxy</name>
    <message>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlayItemWidget</name>
    <message>
        <source>The file does not exist</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlaylistWidget</name>
    <message>
        <source>Playlist</source>
        <translation>Seznam predvajanja</translation>
    </message>
    <message>
        <source>%1 videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Empty</source>
        <translation>Prazno</translation>
    </message>
</context>
<context>
    <name>dmr::Settings</name>
    <message>
        <source>%1/Movie%2.jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1/Movie%2(%3).jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation>Vrstni red predvajanja </translation>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::ToolboxProxy</name>
    <message>
        <source>Play</source>
        <translation>Predvajaj</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>Predhodni</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>Naslednji</translation>
    </message>
    <message>
        <source>Subtitles</source>
        <translation>Podnapisi</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Seznam predvajanja</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Celozaslonski način</translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation>Predvajaj/premor</translation>
    </message>
    <message>
        <source>play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>prev</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>next</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>fs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Exit fullscreen</source>
        <translation>Izhod iz celozaslonskega načina</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Premor</translation>
    </message>
</context>
</TS>