<?xml version="1.0" ?><!DOCTYPE TS><TS language="ast" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <source>Invalid folder</source>
        <translation>La carpeta nun ye válida</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Network disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You don&apos;t have permission to operate this folder</source>
        <translation>Nun tienes permisu pa operar nesta carpeta</translation>
    </message>
    <message>
        <source>Auto add similar files to play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Clear playlist when exit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show video preview on mouseover</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open a new player for each file played</source>
        <translation>Abrir una ventana nueva por cada ficheru que se reproduza</translation>
    </message>
    <message>
        <source>Pause when minimized</source>
        <translation>Posar al minimizar</translation>
    </message>
    <message>
        <source>Remember playback position</source>
        <translation>Recordar posición de reproducción</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Camín</translation>
    </message>
    <message>
        <source>Basic</source>
        <translation>Básicu</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>Reproducir</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Capturar pantalla</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Atayos</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Ficheru</translation>
    </message>
    <message>
        <source>Frame/Sound</source>
        <translation>Cuadros/Soníu</translation>
    </message>
    <message>
        <source>Playback</source>
        <translation>Reproducción</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Font Style</source>
        <translation>Estilu de la fonte</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>Reafitar valores</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Abrir un ficheru</translation>
    </message>
    <message>
        <source>Open next</source>
        <translation>Abrir siguiente</translation>
    </message>
    <message>
        <source>Open previous</source>
        <translation>Abrir previu</translation>
    </message>
    <message>
        <source>Mini mode</source>
        <translation>Mou mini</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Silenciar</translation>
    </message>
    <message>
        <source>Next frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Volume down</source>
        <translation>Baxar volume</translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation>Xubir volume</translation>
    </message>
    <message>
        <source>Speed up</source>
        <translation>Acelerar</translation>
    </message>
    <message>
        <source>Speed down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Panatalla completa</translation>
    </message>
    <message>
        <source>Pause/Play</source>
        <translation>Posar/Reproducir</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Llista de reproducción</translation>
    </message>
    <message>
        <source>Reset speed</source>
        <translation>Refitar velocidá</translation>
    </message>
    <message>
        <source>Rewind</source>
        <translation>Rebobinar</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Avanzar</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>0.5s backward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>0.5s forward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Font</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <source>Exit fullscreen/mini mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Movie</source>
        <translation>Película</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Axustes</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <source>Display shortcuts</source>
        <translation>Amosar atayos</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>Información de película</translation>
    </message>
    <message>
        <source>Burst shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size</source>
        <translation>Tamañu</translation>
    </message>
    <message>
        <source>Movie is a full-featured video player, supporting playing local and streaming media in multiple video formats.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>Aceutar</translation>
    </message>
    <message>
        <source>Default play mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show thumbnails in progress bar</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UrlDialog</name>
    <message>
        <source>Cancel</source>
        <translation>Encaboxar</translation>
    </message>
    <message>
        <source>Please enter the URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>Aceutar</translation>
    </message>
</context>
<context>
    <name>dmr::ActionFactory</name>
    <message>
        <source>Settings</source>
        <translation>Axustes</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Panatalla completa</translation>
    </message>
    <message>
        <source>Always on Top</source>
        <translation>Siempres enriba</translation>
    </message>
    <message>
        <source>Empty playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>Información de película</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Abrir un ficheru</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open URL</source>
        <translation>Abrir una URL</translation>
    </message>
    <message>
        <source>Open CD/DVD</source>
        <translation>Abrir un CD/DVD</translation>
    </message>
    <message>
        <source>Mini Mode</source>
        <translation>Mou mini</translation>
    </message>
    <message>
        <source>Play Mode</source>
        <translation>Mou de reproducción</translation>
    </message>
    <message>
        <source>Order Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shuffle Play</source>
        <translation>Reproducción al debalu</translation>
    </message>
    <message>
        <source>Single Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single Loop</source>
        <translation>Una repitición</translation>
    </message>
    <message>
        <source>List Loop</source>
        <translation>Repitición del llistáu</translation>
    </message>
    <message>
        <source>Frame</source>
        <translation>Marcu</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Por defeutu</translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Counterclockwise</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sound</source>
        <translation>Soníu</translation>
    </message>
    <message>
        <source>Channel</source>
        <translation>Canal</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>Estereu</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>Canal esquierda</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>Canal drecha</translation>
    </message>
    <message>
        <source>Track</source>
        <translation>Pista</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Load</source>
        <translation>Cargar</translation>
    </message>
    <message>
        <source>Online Search</source>
        <translation>Gueta en llinia</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Esbillar</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation>Anubrir</translation>
    </message>
    <message>
        <source>Encodings</source>
        <translation>Codificaciones</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Capturar pantalla</translation>
    </message>
    <message>
        <source>Film Screenshot</source>
        <translation>Captura de película</translation>
    </message>
    <message>
        <source>Burst Shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Llista de reproducción</translation>
    </message>
    <message>
        <source>Film Info</source>
        <translation>Información de película</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display in file manager</source>
        <translation>Amosar nel xestor de ficheros</translation>
    </message>
    <message>
        <source>Next Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open screenshot folder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::BurstScreenshotsDialog</name>
    <message>
        <source>Duration: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resolution: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
</context>
<context>
    <name>dmr::MainWindow</name>
    <message>
        <source>Load successfully</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Load failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buffering...</source>
        <translation>Atroxando nel búfer...</translation>
    </message>
    <message>
        <source>No device found</source>
        <translation>Nun s&apos;alcontraron preseos</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%1)</source>
        <translation>Tolos vídeos (%1)</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>Estereu</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>Canal esquierda</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>Canal drecha</translation>
    </message>
    <message>
        <source>Parse failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mute</source>
        <translation>Silenciar</translation>
    </message>
    <message>
        <source>Volume: %1%</source>
        <translation>Volume: %1%</translation>
    </message>
    <message>
        <source>Track: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle %1: %2s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>delayed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>advanced</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Speed: %1x</source>
        <translation>Velocidá: %1x</translation>
    </message>
    <message>
        <source>Subtitle (*.ass *.aqt *.jss *.gsub *.ssf *.srt *.sub *.ssa *.smi *.usf *.idx)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <source>Saved to</source>
        <translation>Guardóse en</translation>
    </message>
    <message>
        <source>The screenshot is saved</source>
        <translation>Guardóse la captura de pantalla</translation>
    </message>
    <message>
        <source>Failed to save the screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file: %1</source>
        <translation>Ficheru non válidu: %1</translation>
    </message>
    <message>
        <source>No matching online subtitles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot open file or stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file</source>
        <translation>Ficheros non válidos</translation>
    </message>
    <message>
        <source>No video file found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open File</source>
        <translation>Abrir un ficheru</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Taking the screenshots, please wait...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>Aceutar</translation>
    </message>
    <message>
        <source>Please insert a CD/DVD</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reading DVD files...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>4K video may be stuck</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please load the video first</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The CD/DVD has been ejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%2 %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (*)(%2 %1)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfo</name>
    <message>
        <source>%1G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfoDialog</name>
    <message>
        <source>Film info</source>
        <translation>Información de película</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>Resolución</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Triba</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Tamañu</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Camín</translation>
    </message>
    <message>
        <source>Codec info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 kbps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>FPS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 fps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Proportion</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio digit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 bits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sampling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1hz</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MpvProxy</name>
    <message>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlayItemWidget</name>
    <message>
        <source>The file does not exist</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlaylistWidget</name>
    <message>
        <source>Playlist</source>
        <translation>Llista de reproducción</translation>
    </message>
    <message>
        <source>%1 videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Empty</source>
        <translation>Balero</translation>
    </message>
</context>
<context>
    <name>dmr::Settings</name>
    <message>
        <source>%1/Movie%2.jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1/Movie%2(%3).jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::ToolboxProxy</name>
    <message>
        <source>Play</source>
        <translation>Reproducir</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>Previo</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>Siguiente</translation>
    </message>
    <message>
        <source>Subtitles</source>
        <translation>Sotítulos</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Llista de reproducción</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Panatalla completa</translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation>Reproducir/Posar</translation>
    </message>
    <message>
        <source>play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>prev</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>next</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>fs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Exit fullscreen</source>
        <translation>Colar de pantalla completa</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Posar</translation>
    </message>
</context>
</TS>