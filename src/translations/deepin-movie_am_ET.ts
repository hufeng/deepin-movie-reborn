<?xml version="1.0" ?><!DOCTYPE TS><TS language="am_ET" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <source>Invalid folder</source>
        <translation>ዋጋ የሌለው ፎልደር</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Network disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You don&apos;t have permission to operate this folder</source>
        <translation>እርስዎ ይህን ፎልደር ለ መጠቀም በቂ ፍቃድ የለዎትም </translation>
    </message>
    <message>
        <source>Auto add similar files to play</source>
        <translation>በራሱ ተመሳሳይ ፋይሎች ለ ማጫወቻ መጨመሪያ</translation>
    </message>
    <message>
        <source>Clear playlist when exit</source>
        <translation>በምወጣ ጊዜ የ ማጫወቻውን ዝርዝር ማጽጃ</translation>
    </message>
    <message>
        <source>Show video preview on mouseover</source>
        <translation>አይጥ በላዩ ላይ ሲንሳፈፍ የ ቪዲዮ ቅድመ እይታ ማሳያ </translation>
    </message>
    <message>
        <source>Open a new player for each file played</source>
        <translation>ለሚጫወተው እያንዳንዱ ፋይል አዲስ ማጫወቻ መክፈቻ</translation>
    </message>
    <message>
        <source>Pause when minimized</source>
        <translation>በሚያንስ ጊዜ ማስቆሚያ</translation>
    </message>
    <message>
        <source>Remember playback position</source>
        <translation>የ መልሶ ማጫወቻ ቦታ አስታውስ</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>መንገድ</translation>
    </message>
    <message>
        <source>Basic</source>
        <translation>መሰረታዊ</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>ማጫወቻ</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>መመልከቻውን ፎቶ ማንሻ</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>አቋራጭ </translation>
    </message>
    <message>
        <source>File</source>
        <translation>ፋይል</translation>
    </message>
    <message>
        <source>Frame/Sound</source>
        <translation>ክፈፍ/ዽምፅ</translation>
    </message>
    <message>
        <source>Playback</source>
        <translation>በድጋሚ ማጫወቻ</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>ንዑስ አርእስት</translation>
    </message>
    <message>
        <source>Font Style</source>
        <translation>የ ፊደል ዘዴ</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>ነባር እንደ ነበር መመለሻ</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>ፋይል መክፈቻ</translation>
    </message>
    <message>
        <source>Open next</source>
        <translation>የሚቀጥለውን መክፈቻ</translation>
    </message>
    <message>
        <source>Open previous</source>
        <translation>ቀደም ያለውን መክፈቻ</translation>
    </message>
    <message>
        <source>Mini mode</source>
        <translation>በትንሽ ዘዴ</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>መቀነሻ</translation>
    </message>
    <message>
        <source>Next frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Volume down</source>
        <translation>መጠን መቀነሻ</translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation>መጠን መጨመሪያ</translation>
    </message>
    <message>
        <source>Speed up</source>
        <translation>ፍጥነት መጨመሪያ</translation>
    </message>
    <message>
        <source>Speed down</source>
        <translation>ፍጥነት መቀነሻ</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>በሙሉ መመልከቻ ዘዴ</translation>
    </message>
    <message>
        <source>Pause/Play</source>
        <translation>ማጫወቻ/ማስቆሚያ</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>የማጫወቻ ዝርዝር</translation>
    </message>
    <message>
        <source>Reset speed</source>
        <translation>ፍጥነት እንደ ነበር መመለሻ</translation>
    </message>
    <message>
        <source>Rewind</source>
        <translation>ወደ ኋላ ማጠንጠኛ</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>ወደ ፊት</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation>የ ፊልም መመልከቻ ፎቶ ማንሻ</translation>
    </message>
    <message>
        <source>0.5s backward</source>
        <translation>0.5s ወደ ኋላ </translation>
    </message>
    <message>
        <source>0.5s forward</source>
        <translation>0.5s ወደ ፊት</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>ፊደል</translation>
    </message>
    <message>
        <source>Exit fullscreen/mini mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Movie</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Settings</source>
        <translation>ማሰናጃዎች</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>እርዳታ</translation>
    </message>
    <message>
        <source>Display shortcuts</source>
        <translation>አቋራጭ ማሳያ</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>የ ፊልም መረጃ</translation>
    </message>
    <message>
        <source>Burst shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size</source>
        <translation>መጠን</translation>
    </message>
    <message>
        <source>Movie is a full-featured video player, supporting playing local and streaming media in multiple video formats.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>እሺ</translation>
    </message>
    <message>
        <source>Default play mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show thumbnails in progress bar</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UrlDialog</name>
    <message>
        <source>Cancel</source>
        <translation>መሰረዣ</translation>
    </message>
    <message>
        <source>Please enter the URL:</source>
        <translation>እባክዎን ያስገቡ URL:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>እሺ</translation>
    </message>
</context>
<context>
    <name>dmr::ActionFactory</name>
    <message>
        <source>Settings</source>
        <translation>ማሰናጃዎች</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>በሙሉ መመልከቻ ዘዴ</translation>
    </message>
    <message>
        <source>Always on Top</source>
        <translation>ሁል ጊዜ ከ ላይ</translation>
    </message>
    <message>
        <source>Empty playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>የ ፊልም መረጃ</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>ፋይል መክፈቻ</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open URL</source>
        <translation>URL መክፈቻ</translation>
    </message>
    <message>
        <source>Open CD/DVD</source>
        <translation>ሲዲ/ዲቪዲ መክፈቻ</translation>
    </message>
    <message>
        <source>Mini Mode</source>
        <translation>በ ትንሽ ዘዴ</translation>
    </message>
    <message>
        <source>Play Mode</source>
        <translation>ማጫወቻ ዘዴ</translation>
    </message>
    <message>
        <source>Order Play</source>
        <translation>በ ተራ ማጫወቻ</translation>
    </message>
    <message>
        <source>Shuffle Play</source>
        <translation>መበወዣ</translation>
    </message>
    <message>
        <source>Single Play</source>
        <translation>ነጠላ ማጫወቻ</translation>
    </message>
    <message>
        <source>Single Loop</source>
        <translation>ነጠላ ዙር</translation>
    </message>
    <message>
        <source>List Loop</source>
        <translation>ዝርዝር ዙር</translation>
    </message>
    <message>
        <source>Frame</source>
        <translation>ክፈፍ</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>ነባር</translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation>ከ ግራ ወደ ቀኝ</translation>
    </message>
    <message>
        <source>Counterclockwise</source>
        <translation>ከ ቀኝ ወደ ግራ</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>ድምፅ</translation>
    </message>
    <message>
        <source>Channel</source>
        <translation>ጣቢያ</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>ስቴሪዮ</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>የ ግራ ጣቢያ</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>የ ቀኝ ጣቢያ</translation>
    </message>
    <message>
        <source>Track</source>
        <translation>ተረኛ</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>ንዑስ አርእስት</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>መጫኛ</translation>
    </message>
    <message>
        <source>Online Search</source>
        <translation>በ መስመር ላይ መፈለጊያ</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>ይምረጡ</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation>መደበቂያ</translation>
    </message>
    <message>
        <source>Encodings</source>
        <translation>መቀየሪያ</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>መመልከቻውን ፎቶ ማንሻ</translation>
    </message>
    <message>
        <source>Film Screenshot</source>
        <translation>የ ፊልም መመልከቻ ፎቶ ማንሻ</translation>
    </message>
    <message>
        <source>Burst Shooting</source>
        <translation>ፍንዳታ ፎቶ ማንሻ</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>የማጫወቻ ዝርዝር</translation>
    </message>
    <message>
        <source>Film Info</source>
        <translation>የ ፊልም መረጃ</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display in file manager</source>
        <translation>በ ፋይል አስተዳዳሪ እስጥ ማሳያ</translation>
    </message>
    <message>
        <source>Next Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open screenshot folder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::BurstScreenshotsDialog</name>
    <message>
        <source>Duration: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resolution: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save</source>
        <translation>ማስቀመጫ</translation>
    </message>
</context>
<context>
    <name>dmr::MainWindow</name>
    <message>
        <source>Load successfully</source>
        <translation>ተሳክቶ ተጭኗል </translation>
    </message>
    <message>
        <source>Load failed</source>
        <translation>መጫን አልተቻለም</translation>
    </message>
    <message>
        <source>Buffering...</source>
        <translation>በ መጠበቅ ላይ...</translation>
    </message>
    <message>
        <source>No device found</source>
        <translation>ምንም አካል አልተገኘም</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%1)</source>
        <translation>ሁሉንም ቪዲዮዎች (*)</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>ስቴሪዮ</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>የ ግራ ጣቢያ</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>የ ቀኝ ጣቢያ</translation>
    </message>
    <message>
        <source>Parse failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mute</source>
        <translation>መቀነሻ</translation>
    </message>
    <message>
        <source>Volume: %1%</source>
        <translation>መጠን: %1</translation>
    </message>
    <message>
        <source>Track: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle %1: %2s</source>
        <translation>ንዑስ አርእስት %1: %2s</translation>
    </message>
    <message>
        <source>delayed</source>
        <translation>ዘግይቷል</translation>
    </message>
    <message>
        <source>advanced</source>
        <translation>የረቀቀ</translation>
    </message>
    <message>
        <source>Speed: %1x</source>
        <translation>ፍጥነት: %1x</translation>
    </message>
    <message>
        <source>Subtitle (*.ass *.aqt *.jss *.gsub *.ssf *.srt *.sub *.ssa *.smi *.usf *.idx)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View</source>
        <translation>መመልከቻ</translation>
    </message>
    <message>
        <source>Saved to</source>
        <translation>ተቀምጧል ወደ</translation>
    </message>
    <message>
        <source>The screenshot is saved</source>
        <translation>የ መመልከቻው ፎቶ ተቀምጧል </translation>
    </message>
    <message>
        <source>Failed to save the screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file: %1</source>
        <translation>ዋጋ የሌለው ፋይል: %1</translation>
    </message>
    <message>
        <source>No matching online subtitles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot open file or stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file</source>
        <translation>ዋጋ የሌለው ፋይል</translation>
    </message>
    <message>
        <source>No video file found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open File</source>
        <translation>ፋይል መክፈቻ</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation>የ ፊልም መመልከቻ ፎቶ ማንሻ</translation>
    </message>
    <message>
        <source>Taking the screenshots, please wait...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>እሺ</translation>
    </message>
    <message>
        <source>Please insert a CD/DVD</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reading DVD files...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>4K video may be stuck</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please load the video first</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation>በ ተራ ማጫወቻ</translation>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The CD/DVD has been ejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%2 %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (*)(%2 %1)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfo</name>
    <message>
        <source>%1G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfoDialog</name>
    <message>
        <source>Film info</source>
        <translation>የ ፊልም መረጃ</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>ሪዞሊሽን</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>የሚፈጀው ጊዜ</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>አይነት</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>መጠን</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>መንገድ</translation>
    </message>
    <message>
        <source>Codec info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 kbps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>FPS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 fps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Proportion</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio digit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 bits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sampling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1hz</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MpvProxy</name>
    <message>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlayItemWidget</name>
    <message>
        <source>The file does not exist</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlaylistWidget</name>
    <message>
        <source>Playlist</source>
        <translation>የማጫወቻ ዝርዝር</translation>
    </message>
    <message>
        <source>%1 videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Empty</source>
        <translation>ባዶ</translation>
    </message>
</context>
<context>
    <name>dmr::Settings</name>
    <message>
        <source>%1/Movie%2.jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1/Movie%2(%3).jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation>በ ተራ ማጫወቻ</translation>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::ToolboxProxy</name>
    <message>
        <source>Play</source>
        <translation>ማጫወቻ</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>ቀደም ያለው</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>ይቀጥሉ</translation>
    </message>
    <message>
        <source>Subtitles</source>
        <translation>ንዑስ አርእስቶች</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>የማጫወቻ ዝርዝር</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>በሙሉ መመልከቻ ዘዴ</translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation>ማጫወቻ/ማስቆሚያ</translation>
    </message>
    <message>
        <source>play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>prev</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>next</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>fs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Exit fullscreen</source>
        <translation>ከ ሙሉ መመልከቻው ዘዴ መውጫ</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>ማስቆሚያ</translation>
    </message>
</context>
</TS>