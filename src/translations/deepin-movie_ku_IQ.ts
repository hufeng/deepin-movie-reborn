<?xml version="1.0" ?><!DOCTYPE TS><TS language="ku_IQ" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <source>Invalid folder</source>
        <translation>Peldanka nederbasdar</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Network disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You don&apos;t have permission to operate this folder</source>
        <translation>Destûra te tune ye ku di peldankê de guherînê bike</translation>
    </message>
    <message>
        <source>Auto add similar files to play</source>
        <translation>Ji bo lêdanê pelên wekhev bira jixweber bêne tevlîkirin</translation>
    </message>
    <message>
        <source>Clear playlist when exit</source>
        <translation>Dema derketinê lîsteya lêdanê jê bibe</translation>
    </message>
    <message>
        <source>Show video preview on mouseover</source>
        <translation>Dema mişk li ser wê be, bira pêşdîtina vîdyo bê lîstin</translation>
    </message>
    <message>
        <source>Open a new player for each file played</source>
        <translation>Bira her pel di lêderekî din bê lîstin</translation>
    </message>
    <message>
        <source>Pause when minimized</source>
        <translation>Di biçûkkirinê de bisekinîne</translation>
    </message>
    <message>
        <source>Remember playback position</source>
        <translation>Cihê lêdanê bi bîr bîne</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Rê</translation>
    </message>
    <message>
        <source>Basic</source>
        <translation>Bingehîn</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>Lê Bide</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Dîmena dîmenderê</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Kurtebirî</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Pel</translation>
    </message>
    <message>
        <source>Frame/Sound</source>
        <translation>Sehne/Deng</translation>
    </message>
    <message>
        <source>Playback</source>
        <translation>Lîstin</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>Binnivîs</translation>
    </message>
    <message>
        <source>Font Style</source>
        <translation>Şêwaza Tîpa Nivîsê</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>Mîhengên Destpêkê Dîsa Bîne</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Pelê veke</translation>
    </message>
    <message>
        <source>Open next</source>
        <translation>Ya pêşve veke</translation>
    </message>
    <message>
        <source>Open previous</source>
        <translation>Ya paşve veke</translation>
    </message>
    <message>
        <source>Mini mode</source>
        <translation>Moda biçûk</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Bêdeng</translation>
    </message>
    <message>
        <source>Next frame</source>
        <translation>Sehneya pêşve</translation>
    </message>
    <message>
        <source>Previous frame</source>
        <translation>Sehneya paşve</translation>
    </message>
    <message>
        <source>Volume down</source>
        <translation>Deng dîne</translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation>Deng rake</translation>
    </message>
    <message>
        <source>Speed up</source>
        <translation>Lezê zêde bike</translation>
    </message>
    <message>
        <source>Speed down</source>
        <translation>Lezê kêm bike</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Dîmentêr</translation>
    </message>
    <message>
        <source>Pause/Play</source>
        <translation>Bisekinîne/Lê Bide</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Lîsteya Lêxistinê</translation>
    </message>
    <message>
        <source>Reset speed</source>
        <translation>Lezê sifir bike</translation>
    </message>
    <message>
        <source>Rewind</source>
        <translation>Paşve bibe</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Pêşve bibe</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation>Dîmena dîmenderê ya fîlmê</translation>
    </message>
    <message>
        <source>0.5s backward</source>
        <translation>0.5s paşve</translation>
    </message>
    <message>
        <source>0.5s forward</source>
        <translation>0.5s pêşve</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Tîpa Nivîsê</translation>
    </message>
    <message>
        <source>Exit fullscreen/mini mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Movie</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Settings</source>
        <translation>Mîheng</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display shortcuts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>Agahiya Fîlmê</translation>
    </message>
    <message>
        <source>Burst shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Movie is a full-featured video player, supporting playing local and streaming media in multiple video formats.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>Baş e</translation>
    </message>
    <message>
        <source>Default play mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show thumbnails in progress bar</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UrlDialog</name>
    <message>
        <source>Cancel</source>
        <translation>Betal Bike</translation>
    </message>
    <message>
        <source>Please enter the URL:</source>
        <translation>Ji kerema xwe pê li URLym bike:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>Baş e</translation>
    </message>
</context>
<context>
    <name>dmr::ActionFactory</name>
    <message>
        <source>Settings</source>
        <translation>Mîheng</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Dîmentêr</translation>
    </message>
    <message>
        <source>Always on Top</source>
        <translation>Her tim li ser</translation>
    </message>
    <message>
        <source>Empty playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>Agahiya Fîlmê</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Pelê veke</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open URL</source>
        <translation>URLyê Veke</translation>
    </message>
    <message>
        <source>Open CD/DVD</source>
        <translation>CD/DVDyê veke</translation>
    </message>
    <message>
        <source>Mini Mode</source>
        <translation>Moda Biçûk</translation>
    </message>
    <message>
        <source>Play Mode</source>
        <translation>Moda Lêxistinê</translation>
    </message>
    <message>
        <source>Order Play</source>
        <translation>Dora Lêdanê</translation>
    </message>
    <message>
        <source>Shuffle Play</source>
        <translation>Lêdana Tevlihev</translation>
    </message>
    <message>
        <source>Single Play</source>
        <translation>Bi Tenê Lê Bide</translation>
    </message>
    <message>
        <source>Single Loop</source>
        <translation>Çerxa Stranê</translation>
    </message>
    <message>
        <source>List Loop</source>
        <translation>Çerxa Lîsteyê</translation>
    </message>
    <message>
        <source>Frame</source>
        <translation>Sehne</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Destpêkî</translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation>Ber bi hêla saetê ve</translation>
    </message>
    <message>
        <source>Counterclockwise</source>
        <translation>Ber bi berevajiya hêla saetê ve</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Deng</translation>
    </message>
    <message>
        <source>Channel</source>
        <translation>Kanal</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>Cotekanal</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>Kanala çep</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>Kanala rast</translation>
    </message>
    <message>
        <source>Track</source>
        <translation>Stran</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>Binnivîs</translation>
    </message>
    <message>
        <source>Load</source>
        <translation>Bar Bike</translation>
    </message>
    <message>
        <source>Online Search</source>
        <translation>Lêgerîna Serxet</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Hilbijêre</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation>Veşêre</translation>
    </message>
    <message>
        <source>Encodings</source>
        <translation>Kodkirî</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Dîmena dîmenderê</translation>
    </message>
    <message>
        <source>Film Screenshot</source>
        <translation>Dîmendera Fîlmê</translation>
    </message>
    <message>
        <source>Burst Shooting</source>
        <translation>Rêzegirtin</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Lîsteya Lêxistinê</translation>
    </message>
    <message>
        <source>Film Info</source>
        <translation>Agahiya Fîlmê</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display in file manager</source>
        <translation>Di rêvebira dosyeyê de rê bide</translation>
    </message>
    <message>
        <source>Next Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open screenshot folder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::BurstScreenshotsDialog</name>
    <message>
        <source>Duration: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resolution: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MainWindow</name>
    <message>
        <source>Load successfully</source>
        <translation>Barkirin bi ser ket</translation>
    </message>
    <message>
        <source>Load failed</source>
        <translation>Barkirin bi ser neket</translation>
    </message>
    <message>
        <source>Buffering...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No device found</source>
        <translation>Amûr nehat dîtin</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%1)</source>
        <translation>Hemû vîdyo (%1)</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>Cotekanal</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation>Kanala çep</translation>
    </message>
    <message>
        <source>Right channel</source>
        <translation>Kanala rast</translation>
    </message>
    <message>
        <source>Parse failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mute</source>
        <translation>Bêdeng</translation>
    </message>
    <message>
        <source>Volume: %1%</source>
        <translation>Deng: %1%</translation>
    </message>
    <message>
        <source>Track: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle %1: %2s</source>
        <translation>Binnivîs %1: %2s</translation>
    </message>
    <message>
        <source>delayed</source>
        <translation>derengman</translation>
    </message>
    <message>
        <source>advanced</source>
        <translation>pêşketî</translation>
    </message>
    <message>
        <source>Speed: %1x</source>
        <translation>Lez: %1x</translation>
    </message>
    <message>
        <source>Subtitle (*.ass *.aqt *.jss *.gsub *.ssf *.srt *.sub *.ssa *.smi *.usf *.idx)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View</source>
        <translation>Dîtin</translation>
    </message>
    <message>
        <source>Saved to</source>
        <translation>Hat qeydkirin li</translation>
    </message>
    <message>
        <source>The screenshot is saved</source>
        <translation>Dîmena dîmenderê hat qeydkirin</translation>
    </message>
    <message>
        <source>Failed to save the screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file: %1</source>
        <translation>Pelê nederbasdar: %1</translation>
    </message>
    <message>
        <source>No matching online subtitles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot open file or stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No video file found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open File</source>
        <translation>Pelê Veke</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation>Dîmena dîmenderê ya fîlmê</translation>
    </message>
    <message>
        <source>Taking the screenshots, please wait...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>Baş e</translation>
    </message>
    <message>
        <source>Please insert a CD/DVD</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reading DVD files...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>4K video may be stuck</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please load the video first</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The CD/DVD has been ejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%2 %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (*)(%2 %1)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfo</name>
    <message>
        <source>%1G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfoDialog</name>
    <message>
        <source>Film info</source>
        <translation>Agahiya Fîlmê</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Path</source>
        <translation>Rê</translation>
    </message>
    <message>
        <source>Codec info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 kbps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>FPS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 fps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Proportion</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio digit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 bits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sampling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1hz</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MpvProxy</name>
    <message>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlayItemWidget</name>
    <message>
        <source>The file does not exist</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlaylistWidget</name>
    <message>
        <source>Playlist</source>
        <translation>Lîsteya Lêdanê</translation>
    </message>
    <message>
        <source>%1 videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Empty</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::Settings</name>
    <message>
        <source>%1/Movie%2.jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1/Movie%2(%3).jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::ToolboxProxy</name>
    <message>
        <source>Play</source>
        <translation>Lê Bide</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>Paşve</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>Pêşve</translation>
    </message>
    <message>
        <source>Subtitles</source>
        <translation>Binnivîs</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Lîsteya Lêxistinê</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Dîmentêr</translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation>Lêxe/Bisekinîne</translation>
    </message>
    <message>
        <source>play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>prev</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>next</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>fs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Exit fullscreen</source>
        <translation>Ji dîmentêrê derkeve</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Bisekine</translation>
    </message>
</context>
</TS>