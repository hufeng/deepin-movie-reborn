<?xml version="1.0" ?><!DOCTYPE TS><TS language="ja" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <source>Invalid folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Network disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You don&apos;t have permission to operate this folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Auto add similar files to play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Clear playlist when exit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show video preview on mouseover</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open a new player for each file played</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Pause when minimized</source>
        <translation>最小化時に一時停止</translation>
    </message>
    <message>
        <source>Remember playback position</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Path</source>
        <translation>パス</translation>
    </message>
    <message>
        <source>Basic</source>
        <translation>ベーシック</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>再生</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>スクリーンショット</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>ショートカット</translation>
    </message>
    <message>
        <source>File</source>
        <translation>ファイル</translation>
    </message>
    <message>
        <source>Frame/Sound</source>
        <translation>フレーム/音</translation>
    </message>
    <message>
        <source>Playback</source>
        <translation>プレイバック</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Font Style</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>デフォルトを復元</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>ファイルを開く</translation>
    </message>
    <message>
        <source>Open next</source>
        <translation>次を開く</translation>
    </message>
    <message>
        <source>Open previous</source>
        <translation>前を開く</translation>
    </message>
    <message>
        <source>Mini mode</source>
        <translation>ミニモード</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>消音</translation>
    </message>
    <message>
        <source>Next frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Volume down</source>
        <translation>音量を下げる</translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation>音量を上げる</translation>
    </message>
    <message>
        <source>Speed up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Speed down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>フルスクリーンにする</translation>
    </message>
    <message>
        <source>Pause/Play</source>
        <translation>一時停止・再生</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>プレイリスト</translation>
    </message>
    <message>
        <source>Reset speed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Rewind</source>
        <translation>巻き戻し</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>転送</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>0.5s backward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>0.5s forward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Font</source>
        <translation>フォント</translation>
    </message>
    <message>
        <source>Exit fullscreen/mini mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Movie</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>ヘルプ</translation>
    </message>
    <message>
        <source>Display shortcuts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Burst shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size</source>
        <translation>サイズ</translation>
    </message>
    <message>
        <source>Movie is a full-featured video player, supporting playing local and streaming media in multiple video formats.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Default play mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show thumbnails in progress bar</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UrlDialog</name>
    <message>
        <source>Cancel</source>
        <translation>キャンセル</translation>
    </message>
    <message>
        <source>Please enter the URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>dmr::ActionFactory</name>
    <message>
        <source>Settings</source>
        <translation>設定</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>フルスクリーンにする</translation>
    </message>
    <message>
        <source>Always on Top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Empty playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open file</source>
        <translation>ファイルを開く</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open URL</source>
        <translation>URL を開く</translation>
    </message>
    <message>
        <source>Open CD/DVD</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mini Mode</source>
        <translation>最小化モード</translation>
    </message>
    <message>
        <source>Play Mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shuffle Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single Loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List Loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Frame</source>
        <translation>フレーム</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>デフォルト</translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Counterclockwise</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sound</source>
        <translation>サウンド</translation>
    </message>
    <message>
        <source>Channel</source>
        <translation>チャンネル</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>ステレオ</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Right channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Track</source>
        <translation>曲</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Load</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Online Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Select</source>
        <translation>選択</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Encodings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>スクリーンショット</translation>
    </message>
    <message>
        <source>Film Screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Burst Shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Playlist</source>
        <translation>プレイリスト</translation>
    </message>
    <message>
        <source>Film Info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display in file manager</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Next Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open screenshot folder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::BurstScreenshotsDialog</name>
    <message>
        <source>Duration: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resolution: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size: %1</source>
        <translation>サイズ: %1</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>保存</translation>
    </message>
</context>
<context>
    <name>dmr::MainWindow</name>
    <message>
        <source>Load successfully</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Load failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buffering...</source>
        <translation>バッファ中...</translation>
    </message>
    <message>
        <source>No device found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Stereo</source>
        <translation>ステレオ</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Right channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Parse failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mute</source>
        <translation>消音</translation>
    </message>
    <message>
        <source>Volume: %1%</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Track: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle %1: %2s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>delayed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>advanced</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Speed: %1x</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle (*.ass *.aqt *.jss *.gsub *.ssf *.srt *.sub *.ssa *.smi *.usf *.idx)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View</source>
        <translation>表示</translation>
    </message>
    <message>
        <source>Saved to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The screenshot is saved</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Failed to save the screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No matching online subtitles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot open file or stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file</source>
        <translation>無効なファイル</translation>
    </message>
    <message>
        <source>No video file found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Taking the screenshots, please wait...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Please insert a CD/DVD</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reading DVD files...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>4K video may be stuck</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please load the video first</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation>順番に再生</translation>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The CD/DVD has been ejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%2 %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (*)(%2 %1)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfo</name>
    <message>
        <source>%1G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfoDialog</name>
    <message>
        <source>Film info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resolution</source>
        <translation>解像度</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>再生時間</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>種類</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>サイズ</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>パス</translation>
    </message>
    <message>
        <source>Codec info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 kbps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>FPS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 fps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Proportion</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio digit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 bits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sampling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1hz</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MpvProxy</name>
    <message>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlayItemWidget</name>
    <message>
        <source>The file does not exist</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlaylistWidget</name>
    <message>
        <source>Playlist</source>
        <translation>プレイリスト</translation>
    </message>
    <message>
        <source>%1 videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Empty</source>
        <translation>空</translation>
    </message>
</context>
<context>
    <name>dmr::Settings</name>
    <message>
        <source>%1/Movie%2.jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1/Movie%2(%3).jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation>順番に再生</translation>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::ToolboxProxy</name>
    <message>
        <source>Play</source>
        <translation>再生</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>前へ</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>次へ</translation>
    </message>
    <message>
        <source>Subtitles</source>
        <translation>字幕</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>プレイリスト</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>フルスクリーンにする</translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation>再生・一時停止</translation>
    </message>
    <message>
        <source>play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>prev</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>next</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>fs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Exit fullscreen</source>
        <translation>フルスクリーンを解除</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>一時停止</translation>
    </message>
</context>
</TS>