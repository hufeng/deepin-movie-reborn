<?xml version="1.0" ?><!DOCTYPE TS><TS language="af" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <source>Invalid folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Network disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You don&apos;t have permission to operate this folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Auto add similar files to play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Clear playlist when exit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show video preview on mouseover</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open a new player for each file played</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Pause when minimized</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Remember playback position</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Basic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Skermkiekie</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Frame/Sound</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Playback</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Font Style</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open next</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open previous</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mini mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mute</source>
        <translation>Demp</translation>
    </message>
    <message>
        <source>Next frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Volume down</source>
        <translation>Volume af</translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation>Volume op</translation>
    </message>
    <message>
        <source>Speed up</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Speed down</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Volskerm</translation>
    </message>
    <message>
        <source>Pause/Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reset speed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Rewind</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Forward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>0.5s backward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>0.5s forward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Font</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Exit fullscreen/mini mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Movie</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Help</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display shortcuts</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Burst shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <source>Movie is a full-featured video player, supporting playing local and streaming media in multiple video formats.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Default play mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show thumbnails in progress bar</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UrlDialog</name>
    <message>
        <source>Cancel</source>
        <translation>Kanselleer</translation>
    </message>
    <message>
        <source>Please enter the URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>dmr::ActionFactory</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Volskerm</translation>
    </message>
    <message>
        <source>Always on Top</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Empty playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open URL</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open CD/DVD</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mini Mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Play Mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shuffle Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single Loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List Loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Default</source>
        <translation>Standaard</translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Counterclockwise</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sound</source>
        <translation>Klank</translation>
    </message>
    <message>
        <source>Channel</source>
        <translation>Kanaal</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Left channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Right channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Track</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Load</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Online Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Select</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Hide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Encodings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Skermkiekie</translation>
    </message>
    <message>
        <source>Film Screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Burst Shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film Info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display in file manager</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Next Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open screenshot folder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::BurstScreenshotsDialog</name>
    <message>
        <source>Duration: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resolution: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Save</source>
        <translation>Stoor</translation>
    </message>
</context>
<context>
    <name>dmr::MainWindow</name>
    <message>
        <source>Load successfully</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Load failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buffering...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No device found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open folder</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Stereo</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Left channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Right channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Parse failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mute</source>
        <translation>Demp</translation>
    </message>
    <message>
        <source>Volume: %1%</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Track: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle %1: %2s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>delayed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>advanced</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Speed: %1x</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle (*.ass *.aqt *.jss *.gsub *.ssf *.srt *.sub *.ssa *.smi *.usf *.idx)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Saved to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The screenshot is saved</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Failed to save the screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No matching online subtitles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot open file or stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>No video file found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Taking the screenshots, please wait...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Please insert a CD/DVD</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reading DVD files...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>4K video may be stuck</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please load the video first</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The CD/DVD has been ejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%2 %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (*)(%2 %1)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfo</name>
    <message>
        <source>%1G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfoDialog</name>
    <message>
        <source>Film info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resolution</source>
        <translation>Resolusie</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Type</source>
        <translation>Tipe</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <source>Path</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Codec info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 kbps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>FPS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 fps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Proportion</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio digit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 bits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sampling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1hz</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MpvProxy</name>
    <message>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlayItemWidget</name>
    <message>
        <source>The file does not exist</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlaylistWidget</name>
    <message>
        <source>Playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Empty</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::Settings</name>
    <message>
        <source>%1/Movie%2.jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1/Movie%2(%3).jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::ToolboxProxy</name>
    <message>
        <source>Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous</source>
        <translation>Vorige</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <source>Subtitles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Volskerm</translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation>Speel/Pouse</translation>
    </message>
    <message>
        <source>play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>prev</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>next</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>fs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Exit fullscreen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Pause</source>
        <translation>Pouse</translation>
    </message>
</context>
</TS>