<?xml version="1.0" ?><!DOCTYPE TS><TS language="id" version="2.1">
<context>
    <name>QObject</name>
    <message>
        <source>Invalid folder</source>
        <translation>Folder tidak valid</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Buka Folder</translation>
    </message>
    <message>
        <source>Network disconnected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>You don&apos;t have permission to operate this folder</source>
        <translation>Anda tidak mempunyai ijin untuk beroperasi di folder ini</translation>
    </message>
    <message>
        <source>Auto add similar files to play</source>
        <translation>Otomatis tambah berkas yang sama untuk dimainkan</translation>
    </message>
    <message>
        <source>Clear playlist when exit</source>
        <translation>Bersih playlist ketika keluar</translation>
    </message>
    <message>
        <source>Show video preview on mouseover</source>
        <translation>Tampilkan pratinjau video di atas tetikus</translation>
    </message>
    <message>
        <source>Open a new player for each file played</source>
        <translation>Buka pemutar baru untuk setiap berkas yang dimainkan</translation>
    </message>
    <message>
        <source>Pause when minimized</source>
        <translation>Jeda ketika diminimalkan</translation>
    </message>
    <message>
        <source>Remember playback position</source>
        <translation>Mengingat posisi playback</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Jejak</translation>
    </message>
    <message>
        <source>Basic</source>
        <translation>Dasar</translation>
    </message>
    <message>
        <source>Play</source>
        <translation>Putar</translation>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Tangkapan Layar</translation>
    </message>
    <message>
        <source>Shortcuts</source>
        <translation>Pintasan</translation>
    </message>
    <message>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <source>Frame/Sound</source>
        <translation>Bingkai/Suara</translation>
    </message>
    <message>
        <source>Playback</source>
        <translation>Pemutaran</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>Subtitle</translation>
    </message>
    <message>
        <source>Font Style</source>
        <translation>Jenis Huruf</translation>
    </message>
    <message>
        <source>Restore Defaults</source>
        <translation>Pulihkan baku</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Buka berkas</translation>
    </message>
    <message>
        <source>Open next</source>
        <translation>Buka selanjutnya</translation>
    </message>
    <message>
        <source>Open previous</source>
        <translation>Buka sebelumnya</translation>
    </message>
    <message>
        <source>Mini mode</source>
        <translation>Mode Mini</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation>Senyap</translation>
    </message>
    <message>
        <source>Next frame</source>
        <translation>Frame sesudah</translation>
    </message>
    <message>
        <source>Previous frame</source>
        <translation>Frame sebelum</translation>
    </message>
    <message>
        <source>Volume down</source>
        <translation>Turunkan Volume</translation>
    </message>
    <message>
        <source>Volume up</source>
        <translation>Naikkan Volume</translation>
    </message>
    <message>
        <source>Speed up</source>
        <translation>Percepat</translation>
    </message>
    <message>
        <source>Speed down</source>
        <translation>Turun cepat</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Layar Penuh</translation>
    </message>
    <message>
        <source>Pause/Play</source>
        <translation>Jeda/Putar</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Daftar Putar</translation>
    </message>
    <message>
        <source>Reset speed</source>
        <translation>Atur ulang kecepatan</translation>
    </message>
    <message>
        <source>Rewind</source>
        <translation>Mundur</translation>
    </message>
    <message>
        <source>Forward</source>
        <translation>Maju</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation>Tangkapan layar Film</translation>
    </message>
    <message>
        <source>0.5s backward</source>
        <translation>Putar awal 0.5s</translation>
    </message>
    <message>
        <source>0.5s forward</source>
        <translation>Putar maju 0.5s</translation>
    </message>
    <message>
        <source>Font</source>
        <translation>Fon</translation>
    </message>
    <message>
        <source>Exit fullscreen/mini mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Movie</source>
        <translation>Film</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Pengaturan</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Bantuan</translation>
    </message>
    <message>
        <source>Display shortcuts</source>
        <translation>Tampilkan pintasan</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>Info Film</translation>
    </message>
    <message>
        <source>Burst shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size</source>
        <translation>Ukuran</translation>
    </message>
    <message>
        <source>Movie is a full-featured video player, supporting playing local and streaming media in multiple video formats.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Default play mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Show thumbnails in progress bar</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UrlDialog</name>
    <message>
        <source>Cancel</source>
        <translation>Batal</translation>
    </message>
    <message>
        <source>Please enter the URL:</source>
        <translation>Mohon masukan URL:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>dmr::ActionFactory</name>
    <message>
        <source>Settings</source>
        <translation>Pengaturan</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Layar Penuh</translation>
    </message>
    <message>
        <source>Always on Top</source>
        <translation>Selalu di atas</translation>
    </message>
    <message>
        <source>Empty playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Film info</source>
        <translation>Info Film</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Buka berkas</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Buka Folder</translation>
    </message>
    <message>
        <source>Open URL</source>
        <translation>Buka URL</translation>
    </message>
    <message>
        <source>Open CD/DVD</source>
        <translation>Buka CD/DVD</translation>
    </message>
    <message>
        <source>Mini Mode</source>
        <translation>Mode Mini</translation>
    </message>
    <message>
        <source>Play Mode</source>
        <translation>Mode main</translation>
    </message>
    <message>
        <source>Order Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Shuffle Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single Play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single Loop</source>
        <translation>Putar sekali</translation>
    </message>
    <message>
        <source>List Loop</source>
        <translation>Daftar putar</translation>
    </message>
    <message>
        <source>Frame</source>
        <translation>Bingkai</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Bawaan</translation>
    </message>
    <message>
        <source>Clockwise</source>
        <translation>Searah jarum jam</translation>
    </message>
    <message>
        <source>Counterclockwise</source>
        <translation>Searah jarum jam</translation>
    </message>
    <message>
        <source>Sound</source>
        <translation>Suara</translation>
    </message>
    <message>
        <source>Channel</source>
        <translation>Kanal</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>Stereo</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Right channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Track</source>
        <translation>Lagu</translation>
    </message>
    <message>
        <source>Subtitle</source>
        <translation>Subtitle</translation>
    </message>
    <message>
        <source>Load</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Online Search</source>
        <translation>Cari Online</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Pilih</translation>
    </message>
    <message>
        <source>Hide</source>
        <translation>Sembunyi</translation>
    </message>
    <message>
        <source>Encodings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Screenshot</source>
        <translation>Tangkapan Layar</translation>
    </message>
    <message>
        <source>Film Screenshot</source>
        <translation>Tangkapan layar Film</translation>
    </message>
    <message>
        <source>Burst Shooting</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Daftar Putar</translation>
    </message>
    <message>
        <source>Film Info</source>
        <translation>Info Film</translation>
    </message>
    <message>
        <source>Delete from playlist</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Display in file manager</source>
        <translation>Tampilkan dalam manajer berkas</translation>
    </message>
    <message>
        <source>Next Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Previous Frame</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open screenshot folder</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::BurstScreenshotsDialog</name>
    <message>
        <source>Duration: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Resolution: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Size: %1</source>
        <translation>Ukuran: %1</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Simpan</translation>
    </message>
</context>
<context>
    <name>dmr::MainWindow</name>
    <message>
        <source>Load successfully</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Load failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Buffering...</source>
        <translation>Penyanggaan ...</translation>
    </message>
    <message>
        <source>No device found</source>
        <translation>Alat tidak di temukan</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Buka Folder</translation>
    </message>
    <message>
        <source>All videos (%1)</source>
        <translation>Semua video (%1)</translation>
    </message>
    <message>
        <source>Stereo</source>
        <translation>Stereo</translation>
    </message>
    <message>
        <source>Left channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Right channel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Parse failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Mute</source>
        <translation>Senyap</translation>
    </message>
    <message>
        <source>Volume: %1%</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Track: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Subtitle %1: %2s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>delayed</source>
        <translation>Penundaan</translation>
    </message>
    <message>
        <source>advanced</source>
        <translation>Terkini</translation>
    </message>
    <message>
        <source>Speed: %1x</source>
        <translation>Cepat: %1x</translation>
    </message>
    <message>
        <source>Subtitle (*.ass *.aqt *.jss *.gsub *.ssf *.srt *.sub *.ssa *.smi *.usf *.idx)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>View</source>
        <translation>Lihat</translation>
    </message>
    <message>
        <source>Saved to</source>
        <translation>Disimpan ke</translation>
    </message>
    <message>
        <source>The screenshot is saved</source>
        <translation>Tangkapan layar di simpan</translation>
    </message>
    <message>
        <source>Failed to save the screenshot</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file: %1</source>
        <translation>Berkas tidak valid: %1</translation>
    </message>
    <message>
        <source>No matching online subtitles</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Cannot open file or stream</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Invalid file</source>
        <translation>Berkas tidak valid</translation>
    </message>
    <message>
        <source>No video file found</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Open File</source>
        <translation>Buka Berkas</translation>
    </message>
    <message>
        <source>Film screenshot</source>
        <translation>Tangkapan layar Film</translation>
    </message>
    <message>
        <source>Taking the screenshots, please wait...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Please insert a CD/DVD</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Reading DVD files...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>4K video may be stuck</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Please load the video first</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation>Urutan memainkan</translation>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>The CD/DVD has been ejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (%2 %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>All videos (*)(%2 %1)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfo</name>
    <message>
        <source>%1G</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1M</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1K</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MovieInfoDialog</name>
    <message>
        <source>Film info</source>
        <translation>Info Film</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>Resolusi</translation>
    </message>
    <message>
        <source>Duration</source>
        <translation>Durasi</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Tipe</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Ukuran</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Jejak</translation>
    </message>
    <message>
        <source>Codec info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Video CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 kbps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>FPS</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 fps</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Proportion</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodecID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio CodeRate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Audio digit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 bits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1 channels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Sampling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1hz</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::MpvProxy</name>
    <message>
        <source>Internal</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlayItemWidget</name>
    <message>
        <source>The file does not exist</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::PlaylistWidget</name>
    <message>
        <source>Playlist</source>
        <translation>Daftar Putar</translation>
    </message>
    <message>
        <source>%1 videos</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Empty</source>
        <translation>Kosong</translation>
    </message>
</context>
<context>
    <name>dmr::Settings</name>
    <message>
        <source>%1/Movie%2.jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>%1/Movie%2(%3).jpg</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Order play</source>
        <translation>Urutan memainkan</translation>
    </message>
    <message>
        <source>Shuffle play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Single loop</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>List loop</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>dmr::ToolboxProxy</name>
    <message>
        <source>Play</source>
        <translation>Putar</translation>
    </message>
    <message>
        <source>Previous</source>
        <translation>Sebelumnya</translation>
    </message>
    <message>
        <source>Next</source>
        <translation>Selanjutnya</translation>
    </message>
    <message>
        <source>Subtitles</source>
        <translation>Terjemahan</translation>
    </message>
    <message>
        <source>Playlist</source>
        <translation>Daftar Putar</translation>
    </message>
    <message>
        <source>Fullscreen</source>
        <translation>Layar Penuh</translation>
    </message>
    <message>
        <source>Play/Pause</source>
        <translation>Mainkan/Jeda</translation>
    </message>
    <message>
        <source>play</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>prev</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>next</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>fs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <source>Exit fullscreen</source>
        <translation>Keluar layar penuh</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Jeda</translation>
    </message>
</context>
</TS>